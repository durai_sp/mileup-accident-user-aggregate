"""
Dashboard.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 10, 2017

Dashboard class has the functionality to handle trip aggregate and upload it in S3.
"""


from platformutils.utils.time_util import TimeUtil
from platformutils.utils.file_util import FileUtil
from get_user import GetUser
from user_trip_aggregate import UserTripAggregate
from platformutils.s3_utils import S3Utils

class Dashboard:

    def __init__(self, build):
	self.build = build
	self.file_util =FileUtil()
 	self.time =TimeUtil()
	self.get_user = GetUser(self.build, 'us-east-1', 'DEBUG')
	self.user_trip = UserTripAggregate(self.build, 'us-east-1', 'DEBUG')
	self.s3 = S3Utils('us-east-1', 'DEBUG')
	self.bucket = 'policypalacnprod'
	self.current_directory = self.file_util.get_current_directory()

    def handler(self):
	"""
	This method is used to get users list from user_table.
	Process the users list which is from user_table
	Upload the data into S3
        """
	utc = self.time.get_utc_timestamp(1, 'ms')
	path = self.current_directory + "/trip-event-aggregate-{:}-metadata-".format(self.build) + str(self.time.get_date(1)) + ".txt"
	users = self.get_user.get_users(utc)
	for user in users:
	    data = self.user_trip.get_trip_event_data(user, utc)		
	    self.process_data(data, path)
	self.file_util.compress_text_file(path)	
	self.upload_data(path + ".gz")
	#self.file_util.change_directory(self.current_directory)
	#self.file_util.remove_file_start_with_name('trip-event-aggregate-')

    def process_data(self, data, path):
	"""
	This method is used to process dataframe and write it in file.
        :param data: Provide data has to be process dataframe.
        :param path: Provide path has to be write the content.
        """
	if data is None:
	    pass
	else:
	    try:
	    	index = data.index
	    	count = len(index)
	    	for i in range(0, count): 
		    mData = ''
		    for d in data:
	    		mData += str(data[d][i]) + ","
		    self.file_util.write_data(path, mData[:-1] + "\n")		    
	    except:
		pass

    def upload_data(self, path):
	"""
	This method is used to upload data into S3.
        :param path: Provide path has to be load the data.
        """
	bucket_key = 'agero/prod/datapipeline/dynamodb-backup/prod-trip-event/trip-event-aggregates/{:}/trip-event-aggregate-metadata-'.format(self.build) + str(self.time.get_date(1)) + '.txt.gz'
	self.s3.upload_object(path, self.bucket, bucket_key)

