
import numpy as np
from datetime import datetime
import time
import pandas as pd
import boto3
from boto3.dynamodb.conditions import Key, Attr
import ast


def makeNones(lenCol):
    return [None]*lenCol

def getPhone(pdict):
    row = []
    row.append(len(pdict['ph_call_manip_period']['start_times']) + len(pdict['ph_manip_period']['start_times']))
    row.append(pdict['ph_call_manip_time'])
    row.append(pdict['ph_manip_time'])
    row.append(pdict['ph_call_time'])
    row.append(pdict['phEvalTime'])
    return row


def getABC(t):
    dur = t['duration']
    dist = t['distance']
    nevents = len(t['events'])
    bins=[0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    nbins = [0]*8
    abcDur = dur*3600.

    if nevents > 0:
        for ee in t['events']:
            if t['events'][ee]['kind'] == 'linear':
                e = t['events'][ee]['a_max']
                rflag = 1
                for b in range(len(bins)-1):
                    if rflag==1:
                        if (e >= bins[b]) & (e < bins[b+1]):
                            nbins[b] = nbins[b]+1
                            rflag = 0
                if rflag ==1:
                    nbins[len(nbins)-1] = nbins[len(nbins)-1] + 1
    brow = []
    for b in range(len(nbins)):
        brow.append(float(nbins[b]))
    brow.append(abcDur)
    if 'output_type' in t.keys():
        brow.append(t['output_type'])
    else:
        brow.append(0)
    return brow


def getSpeed(geoDat): 
    hways = ['motorway']
    geoDat[['avgSpeed','duration','maxSpeed','postedCount','speeding10', 'speeding15', 'speeding20', 'speeding25','speeding5']] = geoDat[['avgSpeed','duration','maxSpeed','postedCount','speeding10', 'speeding15', 'speeding20', 'speeding25','speeding5']].astype(float)
    geoDat['totSpeed'] = geoDat['duration']*geoDat['avgSpeed']
    geoDat['totSpeeding5'] = geoDat['speeding5']*geoDat['duration']
    geoDat['totSpeeding10'] = geoDat['speeding10']*geoDat['duration']
    geoDat['totSpeeding15'] = geoDat['speeding15']*geoDat['duration']
    geoDat['totSpeeding20'] = geoDat['speeding20']*geoDat['duration']
    geoDat['totSpeeding25'] = geoDat['speeding25']*geoDat['duration']
    dur = geoDat.duration.sum()
    avgSpeed = geoDat.totSpeed.sum()/geoDat.duration.sum()
    totSpeed_5Sec = geoDat['totSpeeding5'].sum()
    totSpeed_10Sec = geoDat['totSpeeding10'].sum()
    totSpeed_15Sec = geoDat['totSpeeding15'].sum()
    totSpeed_20Sec = geoDat['totSpeeding20'].sum()
    totSpeed_25Sec = geoDat['totSpeeding25'].sum()
    totPostedSpeed = geoDat['postedCount'].sum()
    grow = [dur,avgSpeed,totSpeed_5Sec,totSpeed_10Sec,totSpeed_15Sec,totSpeed_20Sec,totSpeed_25Sec,totPostedSpeed]       
    hwyDat = geoDat[geoDat.highway.isin(hways)]
    reswyDat = geoDat[~geoDat.highway.isin(hways)]
    if len(hwyDat) > 0:
        dur = hwyDat.duration.sum()
        avgSpeed = hwyDat.totSpeed.sum()/hwyDat.duration.sum()
        totSpeed_5SecHwy = hwyDat['totSpeeding5'].sum()
        totSpeed_10SecHwy = hwyDat['totSpeeding10'].sum()
        totSpeed_15SecHwy = hwyDat['totSpeeding15'].sum()
        totSpeed_20SecHwy = hwyDat['totSpeeding20'].sum()
        totSpeed_25SecHwy = hwyDat['totSpeeding25'].sum()
        totPostedSpeedHwy = hwyDat['postedCount'].sum()
        hrow = [dur,avgSpeed,totSpeed_5SecHwy,totSpeed_10SecHwy,totSpeed_15SecHwy,totSpeed_20SecHwy,totSpeed_25SecHwy,totPostedSpeedHwy]
    else:
        hrow = [0,None,None,None,None,None,None,None]
    if len(reswyDat) > 0:
        dur = reswyDat.duration.sum()
        avgSpeed = reswyDat.totSpeed.sum()/reswyDat.duration.sum()
        totSpeed_5SecReswy = reswyDat['totSpeeding5'].sum()
        totSpeed_10SecReswy = reswyDat['totSpeeding10'].sum()
        totSpeed_15SecReswy = reswyDat['totSpeeding15'].sum()
        totSpeed_20SecReswy = reswyDat['totSpeeding20'].sum()
        totSpeed_25SecReswy = reswyDat['totSpeeding25'].sum()
        totPostedSpeedReswy = reswyDat['postedCount'].sum()
        rrow = [dur,avgSpeed,totSpeed_5SecReswy,totSpeed_10SecReswy,totSpeed_15SecReswy,totSpeed_20SecReswy,totSpeed_25SecReswy,totPostedSpeedReswy]
    else:
        rrow = [0,None,None,None,None,None,None,None]
    rtrow = grow + hrow + rrow
    return rtrow

def getABC_full(t):
    dur = t['duration']
    dist = t['distance']
    nevents = len(t['events'])
    bins=[0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0]
    nbinsAccel = [0]*8
    nbinsBrake = [0]*8
    nbinsCornerCW = [0]*8
    nbinsCornerCCW = [0]*8
    abcDur = dur*3600.

    if nevents > 0:
        for ee in t['events']:
            if t['events'][ee]['kind'] == 'linear':
                e = t['events'][ee]['a_max']
                rflag = 1
                if t['events'][ee]['sign'] == 1:
                    for b in range(len(bins)-1):
                        if rflag==1:
                            if (e >= bins[b]) & (e < bins[b+1]):
                                nbinsAccel[b] = nbinsAccel[b]+1
                                rflag = 0
                    if rflag ==1:
                        nbinsAccel[len(nbins)-1] = nbinsAccel[len(nbins)-1] + 1
                else:
                    for b in range(len(bins)-1):
                        if rflag==1:
                            if (e >= bins[b]) & (e < bins[b+1]):
                                nbinsBrake[b] = nbinsBrake[b]+1
                                rflag = 0
                    if rflag ==1:
                        nbinsBrake[len(nbins)-1] = nbinsBrake[len(nbins)-1] + 1                
            else:
                e = t['events'][ee]['a_max']
                rflag = 1
                if t['events'][ee]['sign'] == 1:
                    for b in range(len(bins)-1):
                        if rflag==1:
                            if (e >= bins[b]) & (e < bins[b+1]):
                                nbinsCornerCW[b] = nbinsCornerCW[b]+1
                                rflag = 0
                    if rflag ==1:
                        nbinsCornerCW[len(nbins)-1] = nbinsACornerCW[len(nbins)-1] + 1
                else:
                    for b in range(len(bins)-1):
                        if rflag==1:
                            if (e >= bins[b]) & (e < bins[b+1]):
                                nbinsCornerCCW[b] = nbinsCornerCCW[b]+1
                                rflag = 0
                    if rflag ==1:
                        nbinsCornerCCW[len(nbins)-1] = nbinsCornerCCW[len(nbins)-1] + 1                
    brow = []
    for b in range(len(nbinsAccel)):
        brow.append(float(nbinsAccel[b]))
    for b in range(len(nbinsBrake)):
        brow.append(float(nbinsBrake[b]))
    for b in range(len(nbinsCornerCW)):
        brow.append(float(nbinsCornerCW[b])) 
    for b in range(len(nbinsCornerCCW)):
        brow.append(float(nbinsCornerCCW[b]))
    return brow


def getRowDat(i):
    columns = ['tripId','userId','deviceModel','duration','distance','status','transportMode','reasonCode','geoCode','startUtcDtime']
    columns = columns + ['abcStatus', 'phoneUsageStatus','speedingStatus']
    hways = ['motorway']
    
    row = []
    for col in columns:
        try: 
            row.append(i[col])
        except:
            row.append(None)
    if 'phoneUsage' in i:
        try:
            pdict = ast.literal_eval(i['phoneUsage'])
            phoneRow = getPhone(pdict)
        except:
            phoneRow = makeNones(5)
    else:
        phoneRow = makeNones(5)
            
    if 'brakingOutput' in i:
        try:
            abcDict = ast.literal_eval(i['brakingOutput'])
            abcRow = getABC(abcDict)
        except:
            abcRow = makeNones(10)
    else:
        abcRow = makeNones(10)

    
    if 'geoData' in i:
        try:
            speedDat = pd.DataFrame(i['geoData'])
            speedRow = getSpeed(speedDat)
        except:
            speedRow = makeNones(24)
    else:
        speedRow = makeNones(24)
            
    row = row + phoneRow + abcRow + speedRow 
    

    if 'brakingOutput' in i:
        try:
            abcDict = ast.literal_eval(i['brakingOutput'])
            abcRowFull = getABC_full(abcDict)
        except:
            abcRowFull = makeNones(32)
    else:
        abcRowFull = makeNones(32)
    row = row + abcRowFull
                       
    return row



def get_tripEventDatByUserByDateCol(userId,dateCol,build):
    dynamodb = boto3.resource('dynamodb', region_name='us-east-1')
    event_table = 'ppacn-agero-{:}-trip-event'.format(build)
    table = dynamodb.Table(event_table)
    fCol = 'processedTimeUtc'
    colDat = []

    metadat = table.query(IndexName='userId-index', KeyConditionExpression=Key('userId').eq(userId), \
        FilterExpression = Key(fCol).eq(dateCol))
    if len(metadat['Items']) > 0:
        for i in metadat['Items']:   
            row = getRowDat(i)
            row.append(i['processedTimeUtc'])
            colDat.append(row)

        while 'LastEvaluatedKey' in metadat:   
            metadat = table.query(IndexName='userId-index', KeyConditionExpression=Key('userId').eq(userId), \
            	FilterExpression = Key(fCol).eq(dateCol),
                             ExclusiveStartKey=metadat['LastEvaluatedKey'])
            for i in metadat['Items']:   
                row = getRowDat(i)
                row.append(i['processedTimeUtc'])
                colDat.append(row)

        columns = ['tripId','userId','deviceModel','duration','distance','status','transportMode','reasonCode','geoCode','startUtcDtime']
        columns = columns + ['abcStatus', 'phoneUsageStatus','speedingStatus']
        columns = columns + ['nPhoneManips', 'phoneManipCallTime','phoneManipTime', 'phoneCallTime', 'phEvalTime'] + ['abcEvents0_3', 'abcEvents0_4', 'abcEvents0_5','abcEvents0_6','abcEvents0_7','abcEvents0_8','abcEvents0_9','abcEvents1_0','abcEvalTime','abcType']
        columns = columns + ['totspdur','totspspd','totSpeed_5Sec','totSpeed_10Sec','totSpeed_15Sec','totSpeed_20Sec','totSpeed_25Sec','totPostedSpeed','hwydur','hwyspd','totSpeed_5SecHwy','totSpeed_10SecHwy','totSpeed_15SecHwy','totSpeed_20SecHwy','totSpeed_25SecHwy','totPostedSpeedHwy','reswydur','reswyspd','totSpeed_5SecReswy','totSpeed_10SecReswy','totSpeed_15SecReswy','totSpeed_20SecReswy','totSpeed_25SecReswy','totPostedSpeedReswy']   
        columns = columns + ['accelEvents0_3', 'accelEvents0_4', 'accelEvents0_5', 'accelEvents0_6', 'accelEvents0_7', 'accelEvents0_8', 'accelEvents0_9', 'accelEvents1_0']
        columns = columns + ['brakeEvents0_3', 'brakeEvents0_4', 'brakeEvents0_5', 'brakeEvents0_6', 'brakeEvents0_7', 'brakeEvents0_8', 'brakeEvents0_9', 'brakeEvents1_0']
        columns = columns + ['cornerCWEvents0_3', 'cornerCWEvents0_4', 'cornerCWEvents0_5', 'cornerCWEvents0_6', 'cornerCWEvents0_7', 'cornerCWEvents0_8', 'cornerCWEvents0_9', 'cornerCWEvents1_0']
        columns = columns + ['cornerCCWEvents0_3', 'cornerCCWEvents0_4', 'cornerCCWEvents0_5', 'cornerCCWEvents0_6', 'cornerCCWEvents0_7', 'cornerCCWEvents0_8', 'cornerCCWEvents0_9', 'cornerCCWEvents1_0']
        columns = columns + ['processedTimeUtc']
        colDat = pd.DataFrame(colDat, columns = columns)
        colDat = colDat[colDat.status.astype(int) ==5]
        colDat = colDat[(pd.isnull(colDat.transportMode)) | (colDat.transportMode.astype(float) ==1)]
        colDat = colDat[pd.isnull(colDat.reasonCode) | (colDat.reasonCode.astype(float) == 0)]

    return colDat


# after collecting into one
# colDat.to_csv(<outfile_name>, header = True, index=False)
