"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 11, 2017

handle functionality is used to initiate user aggregate for prod environment.
"""

from dashboard import Dashboard

def handler():
    d = Dashboard('prod')
    d.handler()

handler()
