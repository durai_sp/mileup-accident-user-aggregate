"""
File utils.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 10, 2017

FileUtil class has the utils to change directory and remove file from directory.
"""

import os
import gzip

class FileUtil:

    def change_directory(self, path):
	"""
	This method is used to change parent directory.
        :param path: Provide path to change current directory.
        """
	os.chdir(path)

    def remove_file_start_with_name(self, start_name):
	"""
	This method is used to remove a file.
        :param start_name: Provide start_name to be removed from the directory.
        """
	for file in os.listdir("."):
    	    if file.startswith(start_name):
		os.remove(file)

    def write_data(self, path, data):
	"""
	This method is used to write data into file.
        :param path: Provide path to write data.
	:param data: Provide data for writing.
        """
	with open(path, "a") as myFile:
	    myFile.write(data)

    def is_file_available(self, path):
	"""
	This method is used to check the file is available in the given path.
        :param path: Provide path to check file.
	:return Return file is available or not.
        """
	return os.path.isfile(path)    

    def compress_text_file(self, path):
	"""
	This method is used to compress file.
        :param path: Provide path to compress a file.
        """
	if self.is_file_available(path):
    	    f_in = open(path)
    	    f_out = gzip.open(path + '.gz', 'wb')
    	    f_out.writelines(f_in)
    	    f_out.close()
    	    f_in.close()

    def get_current_directory(self):
	"""
	This method is used to get current directory.
	:return Return current directory.        
	"""
	return os.getcwd()
	 
