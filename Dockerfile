FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y python python-pip wget
RUN pip install --upgrade pip
RUN pip install boto3
RUN pip install psycopg2

COPY platformutils /platformutils
ADD handler_prod.py /
ADD dashboard.py /
ADD get_user.py /
ADD user_trip_aggregate.py /


CMD [ "python", "./handler_prod.py"]




