"""
Get User.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 10, 2017

GetUser class has the functionality to get users list from driveraggregate table.
"""

from platformutils.dynamodb_utils import DynamoDBUtils

class GetUser(object):
    
    def __init__(self, build, region_name, level):
	user_table = 'ppacn-agero-{:}-driveraggregate'.format(build)
	self.db = DynamoDBUtils(user_table, region_name, level)    

    def get_users(self, utc_value):
	"""
	This method is used to get users list from user_table.
        :param utc_value: Provide processedTimeUtc value has to be performed query.
        :return Return user list.
        """
	response = self.db.query_table('processedTimeUtc', utc_value, 'processedTimeUtc-index')
	return self.process_user(response, utc_value)

    def process_user(self, response, utc_value):
	"""
	This method is used to process users data from query response.
	:param response: Provide response which gets from query.
        :param utc_value: Provide processedTimeUtc value has to be performed query if last_evaluated_key is available.
        :return Return user list.
        """
	user = []
	while True:
	    for row in response['Items']:
		user.append(str(row.get('userId')))

	    if response.get('LastEvaluatedKey'):
	        response = self.db.query_table('processedTimeUtc', utc_value, 'processedTimeUtc-index', last_evaluated_key = response['LastEvaluatedKey'])
	    else:
		break 	
	return user	

