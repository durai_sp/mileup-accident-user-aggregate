"""
handler.

Author: Durai S
email: dsankaran@agero.com
Date: Jul 10, 2017

handle functionality is used to initiate user aggregate for dev environment.
"""

from dashboard import Dashboard

def handler():
    d = Dashboard('dev')
    d.handler()

handler()
